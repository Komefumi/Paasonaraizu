import { NavLink } from "react-router-dom";
import clsx from "clsx";

import { ClassName, IMenuItem } from "common-types";
import { InvalidArgumentError } from "exceptions";
import classes from "./classes.module.scss";

type SelectionSignallerType = (value: any) => void;

export enum MenuType {
  DISPLAY_TYPE = "display_type",
  CLICK_TYPE = "click_type",
  NAVIGATE_TYPE = "navigate_type",
}

interface PMenuItem {
  menuItem: IMenuItem;
  onSelect?: SelectionSignallerType;
  className?: ClassName;
  menuType: MenuType;
}

const MenuItem = ({
  menuItem,
  onSelect,
  className,
  menuType,
}: PMenuItem) => {
  const { label, value, IconRendering, navConfig } = menuItem;
  const selectionHandler = onSelect
    ? () => {
        onSelect(value);
      }
    : () => {};

  switch (menuType) {
    case MenuType.CLICK_TYPE:
      return (
        <li
          onClick={selectionHandler}
          className={clsx(classes.menuItem, className)}
        >
          <div className={classes.menuLabel}>{label}</div>
          {IconRendering && (
            <div className={classes.iconRendering}>
              <IconRendering />
            </div>
          )}
        </li>
      );
    case MenuType.DISPLAY_TYPE:
      return (
        <li className={clsx(classes.menuItem, className)}>
          <div className={classes.menuLabel}>{label}</div>
          {IconRendering && (
            <div className={classes.iconRendering}>
              <IconRendering />
            </div>
          )}
        </li>
      );
    case MenuType.NAVIGATE_TYPE:
      if (!navConfig) {
        throw new InvalidArgumentError(
          "When MenuType is of a navigable link list, a valid navConfig must be provided."
        );
      }
      const { link, exact } = navConfig;
      return (
        <li className={clsx(classes.menuItem, className)}>
          <NavLink to={link} exact={exact}>
            <div className={classes.menuLabel}>{label}</div>
            {IconRendering && (
              <div className={classes.iconRendering}>
                <IconRendering />
              </div>
            )}
          </NavLink>
        </li>
      );
    default:
      throw new InvalidArgumentError("Must provide a valid MenuType value");
  }
};

export enum MenuOrientation {
  VERTICAL = "vertical",
  HORIZONTAL = "horizontal",
}

interface PMenu {
  className?: ClassName;
  listClassName?: ClassName;
  listItemClassName?: ClassName;
  menuItems: IMenuItem[];
  onSelect?: SelectionSignallerType;
  direction: MenuOrientation;
  menuType: MenuType;
}

const Menu = ({
  className,
  listClassName,
  listItemClassName,
  menuItems,
  onSelect,
  direction,
  menuType,
}: PMenu) => {
  const containerClassName = clsx(
    className,
    classes.container,
  );
  const menuClassName = clsx(
    classes.menu,
    listClassName,
    direction === MenuOrientation.VERTICAL && classes.menuVertical,
    direction === MenuOrientation.HORIZONTAL && classes.menuHorizontal
  );
  return (
    <div className={containerClassName}>
      <ul className={menuClassName}>
        {menuItems.map((currentItem) => (
          <MenuItem
            menuItem={currentItem}
            onSelect={onSelect}
            className={listItemClassName}
            menuType={menuType}
          />
        ))}
      </ul>
    </div>
  );
};

export default Menu;
