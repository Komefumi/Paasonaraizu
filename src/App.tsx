import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import routingConfig from "config/routing";
import { SoftwareEngineeringMistakeException } from "exceptions";

function App() {
  return (
    <div>
      <Router>
        <Switch>
          {routingConfig.map(({ navConfig, value, component }) => {
            if (!navConfig) {
              throw new SoftwareEngineeringMistakeException(
                "Unexpected: Routing Item requires to have a navConfig"
              );
            }
            return (
              <Route
                path={navConfig.link}
                exact={navConfig.exact}
                key={value}
                component={component}
              />
            );
          })}
        </Switch>
      </Router>
    </div>
  );
}

export default App;
