import { FC as ReactFC } from "react";

export type ClassName = string;
export type LinkLocation = string;

export interface NavConfig {
  link: LinkLocation;
  exact: boolean;
  activeClassName?: ClassName;
};

export interface IMenuItem {
  label: string;
  value?: any;
  IconRendering?: ReactFC<any>;
  navConfig?: NavConfig;
}

export interface IMenuItemRouting extends IMenuItem {
  component: ReactFC;
};
