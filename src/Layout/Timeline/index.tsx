import { ReactNode } from "react";

import { Columnizer, Column } from "layout-components/columns";
import AppHeader from "app-components/AppHeader";
import SiteNavSection from "app-components/SiteNavSection";
import ChatSection from "app-components/ChatSection";

import classes from "./classes.module.scss";

interface PTimelineLayout {
  children: ReactNode;
}

const TimelineLayout = ({ children }: PTimelineLayout) => {
  return (
    <div>
      <AppHeader />
      <Columnizer className={classes.content}>
        <Column>
          <SiteNavSection />
        </Column>
        <Column>
          { children }
        </Column>
        <Column>
          <ChatSection />
        </Column>
      </Columnizer>
    </div>
  );
};

export default TimelineLayout;
