import {
  MainStream,
  Groups,
  Pages,
  Videos,
  Photos,
  LiveStreams,
  Roleplaying,
} from "Pages/MainStream";

import { IMenuItemRouting } from "common-types";

const routingConfig: IMenuItemRouting[] = [
  {
    component: MainStream,
    label: "Stream",
    navConfig: { link: "/", exact: true },
    value: "stream",
  },
  {
    component: Groups,
    label: "Groups",
    navConfig: { link: "/groups", exact: true },
    value: "groups",
  },
  {
    component: Pages,
    label: "Pages",
    navConfig: { link: "/pages", exact: true },
    value: "pages",
  },
  {
    component: Videos,
    label: "Videos",
    navConfig: { link: "/videos", exact: true },
    value: "videos",
  },
  {
    component: Photos,
    label: "Photos",
    navConfig: { link: "/photos", exact: true },
    value: "photos",
  },
  {
    component: LiveStreams,
    label: "LiveStreams",
    navConfig: { link: "/live-streams", exact: true },
    value: "live-streams",
  },
  {
    component: Roleplaying,
    label: "Roleplaying",
    navConfig: { link: "/roleplaying", exact: true },
    value: "roleplaying",
  },
];

export default routingConfig;
