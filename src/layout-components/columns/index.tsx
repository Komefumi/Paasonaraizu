import clsx from "clsx";

import { ChildrenWithClassName } from "prop-interfaces/children-with-classname";

import classes from "./classes.module.scss";

export const Columnizer = ({ children, className }: ChildrenWithClassName) => {
  return (
    <div className={clsx(classes.columnizer, className)}>{ children }</div>
  );
};

export const Column = ({ children, className }: ChildrenWithClassName) => {
  return (
    <div className={clsx(classes.column, className)}>{children}</div>
  );
};

