import TimelineLayout from "Layout/Timeline";

export const MainStream = () => {
  return (
    <TimelineLayout>
      <div>Main Stream Content Goes Here</div>
    </TimelineLayout>
  );
};

export const Groups = () => {
  return (
    <TimelineLayout>
      <div>Will Build out groups here</div>
    </TimelineLayout>
  );
};

export const Pages = () => {
  return (
    <TimelineLayout>
      <div>Pages Content Will come here</div>
    </TimelineLayout>
  );
};

export const Photos = () => {
  return (
    <TimelineLayout>
      <div>Will list photos here...</div>
    </TimelineLayout>
  );
};

export const Videos = () => {
  return (
    <TimelineLayout>
      <div>Videos will be shown here</div>
    </TimelineLayout>
  );
};

export const LiveStreams = () => {
  return (
    <TimelineLayout>
      <div>Will display livestreams in this space</div>
    </TimelineLayout>
  );
};

export const Roleplaying = () => {
  return (
    <TimelineLayout>
      <div>Roleplaying content will be made available</div>
    </TimelineLayout>
  );
};

