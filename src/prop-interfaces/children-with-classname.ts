import { ReactNode } from "react";

export interface ChildrenWithClassName {
  children: ReactNode;
  className?: string;
};

export interface ChildrenWithClassNameStrict extends ChildrenWithClassName {
  className: string;
};

