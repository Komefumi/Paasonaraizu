import Menu, { MenuType, MenuOrientation } from "ui-components/Menu";
import routingConfig from "config/routing";

import classes from "./classes.module.scss";

const SiteNavSection = () => {
  return (
    <div className={classes.container}>
      <Menu
        menuItems={routingConfig}
        direction={MenuOrientation.VERTICAL}
        menuType={MenuType.NAVIGATE_TYPE}
        listItemClassName={classes.menuItem}
      />
    </div>
  );
};

export default SiteNavSection;
