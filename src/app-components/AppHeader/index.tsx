import clsx from "clsx";

import classes from "./classes.module.scss";
import utilClasses from "styling/modules/layout-utils.module.scss";

const AppHeader = () => {
  return (
    <div className={clsx(classes.container, utilClasses.container)}>
      <div className={classes.title}>Paasonaraizu !</div>
    </div>
  );
};

export default AppHeader;
